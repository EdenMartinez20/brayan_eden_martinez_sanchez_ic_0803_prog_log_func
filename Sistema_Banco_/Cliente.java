public class Cliente {
    
    private double saldo;
    private boolean ocupado;
    private Persona []personas;
   
    Cliente(){
        this.setTodosDatos();
    }

    Cliente(String nombre, char sexo, double saldo){
        this.setTodosDatos(nombre, sexo, saldo);
         
    }
    //, Persona persona
    

    Cliente(Cliente cliente){
        this.setTodosDatos(cliente);
        
    }

    private void setTodosDatos(){
       Persona persona=new Persona();
       persona.setTodosDatosP(null, ' ');
        this.saldo = 0.0;
        this.ocupado = false;
    }

    public void setTodosDatos( String nombre, char sexo,double saldo){
        Persona persona=new Persona();
        persona.setTodosDatosP(nombre,sexo);
        this.validarSaldo(saldo);
        this.setOcupado();
    }

    public void setTodosDatos(Cliente cliente){
        Persona persona=new Persona();
        persona.setTodosDatosP(persona.getNombre(),persona.getSexo());
        this.validarSaldo(cliente.getSaldo());
        this.setOcupado();
    }

    public Cliente getTodosDatos(){
        Cliente temporal = new Cliente();
        Persona temporal2=new Persona();
        temporal2.getTodosDatosP();
         temporal.saldo = this.getSaldo();
        temporal.ocupado = this.isOcupado();
        return temporal;
    }
    
       
       
   
    

    
    private void validarSaldo(double saldo){
        if(saldo <= 0.0){
            saldo = 0.0;
        }
        this.saldo = saldo;
    }

    private boolean tieneSaldo(){
        return this.saldo > 0.0;
    }

    private boolean tieneDatos(){
        Persona pe=new Persona();
                      // return  pe.tieneDatosP();
                return !(pe.getNombre()== null ||pe.getNombre().equalsIgnoreCase(" ") || pe.getSexo()== ' ');
    }
     
        /*if(this.nombre == null || this.nombre.equalsIgnoreCase(" ") || this.sexo == ' ')
            return false;
        else
            return true;
    }*/

    private void setOcupado() {
        if(!this.tieneDatos()){
            this.setTodosDatos();
        }
        else{
            this.ocupado = true;
        }
    }

    

    public double getSaldo() {
        return saldo;
    }

    public boolean isOcupado() {
        return ocupado;
    }

    public void eliminar(){
        this.setTodosDatos();
    }

   

    public void abonarSaldo(double cuanto){
        if(this.tieneDatos()) {
            if(cuanto > 0.0){
                this.saldo += cuanto;
            }
            else{
                System.out.println("\tImposible, no puedes abonar saldos negativos o nada.");
            }
        }
    }

    public void retirarSaldo(double cuanto){
        if(this.tieneDatos()){
            if(cuanto > 0.0){
                if(this.tieneSaldo()){
                    if(this.saldo >= cuanto)
                        this.saldo -= cuanto;
                    else
                        System.out.println("\tImposible, tú saldo es insuficiente para retirar " + cuanto + ".");
                }
                else
                    System.out.println("\tImposible, no hay saldo.");
            }
            else
                System.out.println("\tImposible, no puedes retirar saldos negativos o nada.");
        }
    }

    @Override
    public String toString(){
        return "-> Saldo: |" + this.saldo +
                "| -> Ocupado: |" + this.ocupado + "|";
    }

    public void imprimirCliente(){
        Persona p=new Persona();
        p.imprimirPersona();
        
        System.out.println("\t\t" +p+ this.toString() );
        
    }
    
 
   
           
           }