import java.util.List;

public class FP_Functional_Exercises{

	public static void main (String[] args){
		List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);

		List<String> courses = List.of("Spring", "Spring Boot", "API", "Microservices", "AWS", "PCF", "Azure", "Docker", "Kubernetes");

	System.out.println("EJERCICIO 1 --> Imprimir solo numeros impares de la lista");
	System.out.println("\tForma 1.1");
	numbers.stream()
	.filter(numeros -> numeros % 2 !=0)
	.forEach(numeros -> System.out.println(" "+numeros));

	System.out.println("\n\tForma 1.2");
	numbers.stream()
	.filter(FP_Functional_Exercises::numeroImpar)
	.forEach(numeros -> System.out.println(" "+numeros));

	System.out.println("\n\tForma 1.3");
	numbers.stream()
	.filter(FP_Functional_Exercises::numeroImpar)
	.forEach(FP_Functional_Exercises::print);

	System.out.println("\n");

	System.out.println("EJERCICIO 2 --> Imprimir todos los cursos individualmente");
	System.out.println("\tForma 2.1");
	courses.stream().forEach(le -> System.out.print(le + ", "));

	System.out.println("\n\tForma 2.2");
	courses.stream().forEach(FP_Functional_Exercises::print);

	System.out.println("\n");

	System.out.println("EJERCICIO 3 --> Cursos impresos que contienen la palabra Spring");
	courses.stream()
	.filter(palabra-> palabra.contains("Spring"))
	.forEach(FP_Functional_Exercises::print);

	System.out.println("\n");
	
	System.out.println("\nEJERCICIO 4 --> Cursos impresos cuyo nombre tenga al menos 4 letras");
	courses.stream()
	.filter(course -> course.length() >= 4)
	.forEach(FP_Functional_Exercises::print);
	System.out.println("\n");	

	System.out.println("\nEJERCICIO 5 --> Imprime los cubos de numeros impares");
	numbers.stream()
	.filter(number -> number % 2 != 0)
	.map(number -> number * number * number)
	.forEach(FP_Functional_Exercises::print);
	System.out.println("\n");

	System.out.println("\nEJERCICIO 6 --> Imprime el numero de caracteres en el nombre de cada curso");
	courses.stream()
	.map(course -> course + " = " + course.length())
	.forEach(FP_Functional_Exercises::print);
	System.out.println("");		

	System.out.println("\n");

}
	private static boolean numeroImpar(int entero){
		return (entero % 2 !=0);
	}

	private static void print(int numero){
		System.out.print(numero + ", ");
	}

	private static void print(String palabra){
		System.out.print(palabra + ", ");
	}

}