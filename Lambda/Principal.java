public class Principal {
	public static void main(String[] args){
		Principal.engine((long x, long y) -> x + y);
		Principal.engine((int x, int y) -> x * y);
		Principal.engine((int x, int y) -> x / y);
		Principal.engine((int x, int y) -> x - y);
		
		Principal.engine((long x, long y) -> x % y);
	}

	//Sobrecarga de Métodos
	private static void engine(calculadoraint cal){
		int x = 2, y = 4;
		int resultado=cal.calcular(x, y);
		System.out.println("Resultado INT 5,6,7 = " + resultado);
	}

	private static void engine(calculadoraLong cal){
		long x = 2, y = 4;
		long resultado = cal.calcular(x, y);
		System.out.println("Resultado LONG 3,9 = " + resultado);
	}
}
